class CustomError extends Error {

    constructor(code, title, description) {
        super(`{"code": ${code}, "message": "${title}", "description": "${description}"}`);
        this.code = code;
        this.title = title;
        this.description = description;
    }
}

module.exports = CustomError;