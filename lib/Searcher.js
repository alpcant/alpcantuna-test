

const Check = require("../lib/Check");
const Error = require("../lib/Error");
const Enum = require("../config/Enum");
const GetirModel = require("../db/models/GetirModel");

let instance = null;

class Searcher {

    constructor() {

        if (!instance) instance = this;
        return this;
    }

    /**
     * Search the records on the cloud
     * 
     * @param {Date} startDate
     * @param {Date} endDate
     * @param {Number} minCount
     * @param {Number} maxCount
     * 
     * @return {Array} records
     */
    async recordFounder(startDate, endDate, minCount, maxCount) {

        try {
            return await GetirModel.aggregate([
                {
                    $project: {
                        _id: false,
                        key: "$key",
                        createdAt: "$createdAt",
                        totalCount: { $sum: "$counts" }
                    }
                },
                {
                    $match: {
                        totalCount: {
                            "$gte": minCount,
                            "$lte": maxCount
                        },
                        createdAt: {
                            "$gte": startDate,
                            "$lte": endDate
                        }
                    }
                }])

        } catch (error) {
            throw error;
        }
    }
}

module.exports = Searcher;