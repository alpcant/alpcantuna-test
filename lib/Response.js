const CustomError = require("./Error");
const Enum = require("../config/Enum");

/* Singleton */

let instance = null;

class Response {
    constructor() {

        if (!instance) instance = this;

        return this;
    }

    generateResponse(code, dataKey, dataValue) {
        return {
            code,
            msg: "Success",
            [dataKey]: dataValue
        }
    }
    generateError(error) {
        if (error instanceof CustomError) {

            return {
                code: error.code,
                msg: error.title,
                reason: error.description
            }
        }
        return {
            code: Enum.HTTP_CODES.INT_SERVER_ERROR,
            msg: "Server Error",
            reason: "Server is temporarily unavailable, please try again later."
        }
    }
}

module.exports = Response;
