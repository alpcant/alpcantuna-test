/**
 * Validations, controls handling here
 */

let instance = null;

/* SINGLETON CLASS */
class Check {

    constructor() {
        if (!instance) {
            instance = this;
            this._dayCountOfMonths = [31, 28 + (new Date().getFullYear() % 4 == 0 ? 1 : 0), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        }
        return instance;
    }

    get dayCountOfMonths() {
        return this._dayCountOfMonths;
    }
    set dayCountOfMonths(data) {
        this._dayCountOfMonths = data;
    }


    /**
     * Checks for empty
     * 
     * @param {Any} data
     * @return {Boolean}
     */
    isEmpty(data) {
        if (data == null || data == undefined || data === "")
            return true;
        return false;
    }

    /**
     * Checks for string
     * 
     * @param {String} str
     * @return {Boolean}
     */
    isString(str) {
        if (this.isEmpty(str) || typeof str != "string") return false;
        return true;
    }

    /**
     * Checks for string
     * 
     * @param {String} str
     * @return {Boolean}
     */
    isNumber(num) {
        if (this.isEmpty(num) || typeof num != "number") return false;
        return true;
    }

    /**
     *  Checks the valid date format
     * 
     * @param {String} date "2019-11-27"
     * @return {Boolean}
     */
    isValidDateFormat(date) {

        if (this.isEmpty(date) || !this.isString(date))
            return false;

        /* YYYY-MM-DD */

        if (/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(date)) {

            let dateArr = date.split("-").map(x => parseInt(x));

            if (!/^20/.test(dateArr[0])) return false;

            if (dateArr[1] < 1 || dateArr[1] > 12)
                return false;

            if (dateArr[2] < 1 || this.dayCountOfMonths[dateArr[1] - 1] < dateArr[2])
                return false;
        }
        else {
            return false;
        }
        return true;
    }

    /**
     * Parameter validation
     * 
     * @param {Object} source to be validated obj
     * @param {Array} target to be checked array
     * @return {Boolean}
     * 
     */
    isParametersValid(source = {}, ...arr) {

        if (arr.length != Object.keys(source).length) return false;

        if (Object.keys(source).every(x => arr.indexOf(x) > -1)) return true;

        return false;

    }



}


module.exports = Check;