const mongoose = require("mongoose");

/**
 * records Schema
 **/

const schema = mongoose.Schema({
    key: { type: mongoose.Schema.Types.String },
    value: { type: mongoose.Schema.Types.String },
    createdAt: { type: mongoose.Schema.Types.Date },
    counts: { type: [mongoose.Schema.Types.Number] },

})

// schema.index({ key: 1, value: 1 }, { unique: true });

class Records extends mongoose.Model {

    static async find(query, projection, next) {

        try {

            return await super.find(query, projection);

        } catch (error) {

        }
    }

}

schema.loadClass(Records)
module.exports = mongoose.model("records", schema);