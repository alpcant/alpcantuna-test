
const mongoose = require("mongoose");

mongoose.Promise = Promise;

let instance = null;

class Mongo {
    constructor() {
        if (!instance)
            instance = this;
        return instance;
    }

    connect(options, next) {
        if (!next) next = () => { }
        console.log("MongoDB Connecting!");
        mongoose.connect(options.connectionString).then(res => {
            console.log("MongoDB Connection Established!");
            next()

        }).catch(err => {
            console.log("MongoDB Connection Failed!!!", err);
            next(err)

        });
    }

    find(model, query = {}, options) {
        model = require("../models/" + model);
        return model.find(query, options);
    }


}


module.exports = new Mongo();