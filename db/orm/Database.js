
/* Singleton */

let instance = null;

class Database {
    constructor(dbType) {
        if (!instance) {
            this.database = require("./" + dbType);
            instance = this;   
        }
        return instance;
    }

    connect(options, next) {
        this.database.connect(options, next);
    }

    find(model, query, options) {
        return this.database.find(model, query, options);
    }
}

module.exports = Database;