var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var Database = require('./db/orm/Database');
var config = require('./config/index');
var Enum = require('./config/Enum');
var Error = require('./lib/Error');
var Response = require('./lib/Response');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


new Database(config.DB_TYPE).connect(config[config.DB_TYPE], (err) => {
  if (!err)
    app.use('/', indexRouter);
  else
    app.use('/', function (req, res, next) {
      let response = new Response().generateError(new Error(Enum.HTTP_CODES.FORBIDDEN, "Connection Failed!", "Mongo connection failed!"))
      res.status(response.code).json(response)
    });
  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    // next(createError(Enum.HTTP_CODES.NOT_FOUND));

    let response = new Response().generateError(new Error(Enum.HTTP_CODES.NOT_FOUND, "Not Found", "Page not found!"))
    res.status(response.code).json(response)
  });
})



// error handler
app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  let response = new Response().generateError(err);
  res.status(response.code).json(response);
});

module.exports = app;
