/**
 * 
 * Source: https://www.npmjs.com/package/http-status-codes
 * 
**/

module.exports = {

    HTTP_CODES: {
        GETIR_SUCCESS: 0,
        OK: 200,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        UNPROCESSABLE_ENTITY: 422,
        INT_SERVER_ERROR: 500
    }


}
