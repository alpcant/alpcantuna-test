var express = require('express');
var router = express.Router();

const Enum = require("../config/Enum");
const Error = require("../lib/Error");
const Response = require("../lib/Response");
const Check = require("../lib/Check");
const Searcher = require("../lib/Searcher");

/**
 * Get the records
 * 
 * body {
 *  startDate: date
 *  endDate: date
 *  minCount: number
 *  maxCount: number
 * }
 */
router.post('/', async (req, res, next) => {
  let data = req.body;
  try {

    if (!new Check().isParametersValid(req.body, "startDate", "endDate", "minCount", "maxCount"))
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "Request parameters is not valid");

    if (!new Check().isValidDateFormat(data.startDate))
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "'startDate' is not valid date format");

    if (!new Check().isValidDateFormat(data.endDate))
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "'endDate' is not valid date format");

    if (new Date(data.startDate) > new Date(data.endDate))
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "Start date cannot bigger than End date")

    if (!new Check().isNumber(data.minCount))
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "'minCount' is not a number");

    if (!new Check().isNumber(data.maxCount))
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "'maxCount' is not a number");

    if (data.minCount > data.maxCount)
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "'minCount' property cannot bigger than 'maxCount' property")

    let startDate = new Date(data.startDate);
    let endDate = new Date(new Date(data.endDate).setDate(new Date(data.endDate).getDate() + 1));
    let minCount = parseInt(data.minCount);
    let maxCount = parseInt(data.maxCount);

    let records = await new Searcher().recordFounder(startDate, endDate, minCount, maxCount);

    res.status(Enum.HTTP_CODES.OK).json(new Response().generateResponse(Enum.HTTP_CODES.GETIR_SUCCESS, "records", records))

  } catch (error) {
    let response = new Response().generateError(error)
    res.status(response.code).json(response)
  }
});

module.exports = router;
